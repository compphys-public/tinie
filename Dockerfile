FROM python:3.7-stretch

# Metadata
LABEL maintainer="janne+tinie.docker@solanpaa.fi"
LABEL vcs-ref=${GIT_COMMIT}

WORKDIR "/root"

RUN DEBIAN_FRONTEND=noninteractive

########################################################################
##########         Install prerequisites                  ##############
########################################################################

RUN apt -qq update && apt -qq upgrade -y \
    && apt -qq install -y --no-install-recommends curl \
        file wget libopenmpi-dev mpi-default-bin \
        build-essential gfortran libhdf5-mpi-dev

RUN CC="mpicc" pip3 install mpi4py

# Install MPI-enabled h5py
RUN CC="mpicc" HDF5_MPI="ON"\
    HDF5_DIR=/usr/lib/x86_64-linux-gnu/hdf5/openmpi/ \
    pip3 install --no-binary=h5py h5py

COPY ./requirements.txt /root/requirements.txt

# Install tinie's requirements
RUN pip3 install -r requirements.txt

# Install nose2 for tests
RUN pip3 install nose2 mypy

CMD ["/bin/bash"]
