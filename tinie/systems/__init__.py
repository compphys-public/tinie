"""
A package for calculating transport properties of a two-dimensional
nanostructure connected to multiple leads.
"""

__author__ = "Rostislav Duda, Joonas Keski-Rahkonen, Janne Solanpää"
__copyright__ = (
    "Copyright 2018-2019, Rostislav Duda, Joonas Keski-Rahkonen & Janne Solanpää"
)
__author_email__ = "rostislav.duda@tuni.fi"
__license__ = "Boost Software License 1.0"
__version__ = "0.9"

from tinie.systems.read_write.system import System
