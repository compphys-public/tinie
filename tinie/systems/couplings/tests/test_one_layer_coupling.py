"""
Tests for single layer coupling.
"""
import unittest

import numpy as np
from tinie.systems.couplings.one_layer_coupling import OneLayerCoupling


class TestOneLayerCoupling(unittest.TestCase):
    ...
