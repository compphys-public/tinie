"""
A package for calculating transport properties of a two-dimensional
nanostructure connected to multiple leads.
"""

__author__ = "Rostislav Duda, Joonas Keski-Rahkonen, Janne Solanpää"
__copyright__ = ("Copyright 2019-2020, Rostislav Duda, Joonas Keski-Rahkonen & Janne Solanpää")
__author_email__ = "rostislav.duda@tuni.fi"
__license__ = "MIT"
__version__ = "1.0.7"

from tinie.main_routines.calculator import Calculator
